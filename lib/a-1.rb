# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  #get the range of nums and create consecutive
  range = (nums.first..nums.last).to_a
  #filter that range with nums not in nums arg
  range.select {|num| !nums.include?(num)}
end

# Write a method that given a letters representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  #split to single digits
  digits = binary.split('').map{|str| str.to_i}.reverse 
  #loop through multiply by 2 raised to index and add to result
  result = 0
  digits.each_with_index do |num, i|
    result += (num *(2**i))
  end
  result
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    results = {}
    self.each do |k, v|
      results[k] = v if yield(k, v)
    end
    results
  end

end



  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  class Hash
    def my_merge(other_hash, &prc)
      #set default proc
      prc ||= Proc.new {|key, old_value, new_value| new_value }

      results = Hash.new

      #first create a copy of hash1
      self.each do |k, v|
        results[k] = v
      end
      #second, iterate through hash2
      other_hash.each do |k, v|
        #this simply adds key values that dont overlap
        if results[k].nil?
          results[k] = v
        else
          #this toggles between old and new
          results[k] = prc.call(k, results[k], v)
        end
      end
      results
    end
  end


# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  return 2 if n == 0
  #get absolute of n
  absolute = n.abs
  negative = false
  negative = true if n < 0
  # check if n is negative
  even = n.even?
  #check if n is even
  # create positive range until n+1 == last element
  sequence = [2, 1]

  until absolute + 1 == sequence.length

    next_num = sequence[-2] + sequence[-1]
    sequence << next_num

  end

  return sequence.last unless negative
  # if positive, return that last number
  return sequence.last if negative && even
  # if negative and even return last number
  0-(sequence.last)
  # else return the last number in negative form

end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)

  #get all substrings
  substrings = get_substrings(string)
  palindromes = substrings.select {|substring| palindrome?(substring)}
  palindromes.sort! {|a, b| a.length <=> b.length}
  longest = palindromes.last

  return false if longest.length <= 2

  longest.length
  #filter out all non palindromes
  # sort_by length get longest
  # return false if longest.length <= 2
end




def palindrome?(str)

  str == str.reverse

end

def get_substrings(string)
  letters = string.split('')
  results = []

  i = 0
  while i < letters.length
    j = i
    while j < letters.length
      substring = letters[i..j].join('')
      results << substring
      j += 1
    end
    i += 1
  end
  results
end

p longest_palindrome("181847117432")
